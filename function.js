// 1.)
// Create a variable number that will store the value of the number provided by the user via the prompt.
// Create a for loop that will be initialized with the number provided bythe user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
// Create a condition that if the current value is less than or equal to 50, stop the loop.
// Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
// Create another condition that if the current value is divisible by 5, print the number.

let number = parseInt(prompt("Please enter a number:"));
console.log("The number you provided is " + number + ".");

for (let i = number; i >= 0; i--)
{
	if (i <= 50)
  	{
  		console.log("The current value is at 50. Terminating the loop!")
    	break;
  	}
  	else if (i % 10 === 0)
  	{
    	console.log("The number is divisible by 10. Skipping the number.");
    	continue;
  	}
  	else if (i % 5 === 0)
  	{
    	console.log(i);
	}
}


// 2.)
// Create a variable that will contain the string
// supercalifragilisticexpialidocious.
// Create another variable that will store the consonants from the string.
// Create another for Loop that will iterate through the individual letters of the string based on it’s length.
// Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
// Create an else statement that will add the letter to the second variable.

let str = "supercalifragilisticexpialidocious";
let consonants = "";

for (let i = 0; i < str.length; i++)
{	
	let letter = str[i];
 	
 	if(
 		str[i].toLowerCase() == "a" ||
 		str[i].toLowerCase() == "e" ||
 		str[i].toLowerCase() == "i" ||
 		str[i].toLowerCase() == "o" ||
 		str[i].toLowerCase() == "u"     
 		)
 	{
    	continue;
	}
	else
	{
    	consonants += letter;
  	}
}

console.log(str);
console.log(consonants);